// import { NavigatorScreenParams } from "@react-navigation/native";

export type AppStackParamList = {
  // 'undefined' means that no parameters are specified
  Home: undefined;
  "My Calendar": undefined;
  Attendees: undefined;
  Browse: undefined;
  Organizer: undefined;
};

export type OrganizerTabParamList = {
  Organizer: undefined;
  "Register Id": undefined;
  "Add Bio": undefined;
  "Add Ideas": undefined;
};
